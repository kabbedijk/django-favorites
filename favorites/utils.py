from django.http import HttpResponse
from django.utils import simplejson
from favorites import settings as fav_settings


def build_message(count):
    if count == 0:
        return ''
    if count == 1:
        return fav_settings.FAV_COUNT_SINGLE % 1
    if int(str(count)[-1:]) in fav_settings.FAV_COUNT_PLURAL_SPECIAL_LASTNUMBERS:
        return fav_settings.FAV_COUNT_PLURAL_SPECIAL % count
    return fav_settings.FAV_COUNT_PLURAL % count


def ajax_login_required(view_func):
    def wrap(request, *args, **kwargs):
        if request.user.is_authenticated():
            return view_func(request, *args, **kwargs)
        json = simplejson.dumps({'not_authenticated': True})
        return HttpResponse(json, mimetype='application/json', status=401)
    wrap.__doc__ = view_func.__doc__
    wrap.__dict__ = view_func.__dict__
    return wrap
